import UIKit

class SecondViewController: UIViewController, UITextFieldDelegate {
    
    // store the text in text field
    @IBOutlet weak var toDoItem: UITextField!
    
    // instructions running when "add" button is pressed
    @IBAction func addItem(sender: AnyObject) {
        
        // hide keyboard when add button is pressed
        self.view.endEditing(true)
        
        // no action if text field is empty
        if toDoItem.text != "" {
            
            // add text field content to array
            toDoItems.append(toDoItem.text)
        
            // immutable array created for persistent storage
            let fixedToDoItems = toDoItems
       
            // store the array in memory represented by key "List"
            NSUserDefaults.standardUserDefaults().setObject(fixedToDoItems, forKey: "List")
            NSUserDefaults.standardUserDefaults().synchronize()
            toDoItem.text = ""
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        
        // hide keyboard when return key is pressed
        textField.resignFirstResponder()
        return true
    }
}