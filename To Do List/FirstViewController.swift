import UIKit

var toDoItems = [String]() // global definition of array to store list items

class FirstViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    // outlet of table view to edit view and reload table
    @IBOutlet weak var tasksTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()        
    }
    
    // func to tell the no. of rows in table
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // return the number of elements in array containing list of items
        return toDoItems.count
    }
    
    // func to display the content in table
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // cell initialised
        var cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: "cell")
        
        // storing cell with the contents of table
        cell.textLabel?.text = toDoItems[indexPath.row]
        return cell
    }
    
    // store the table contents at the time of loading app
    override func viewWillAppear(animated: Bool) {
        
        // if persistent memory is non empty then load it into the array
        if var storedToDoItems: AnyObject = NSUserDefaults.standardUserDefaults().objectForKey("List") {
            
            // emptying the array
            toDoItems = []
            // storing contents from memory to array
            for var i = 0; i < storedToDoItems.count ; ++i {
                toDoItems.append(storedToDoItems[i] as String)
            }
        }
        // reload table
        tasksTable.reloadData()
    }
    
    // delete a content
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath){
        
        // if delete is selected
        if editingStyle == UITableViewCellEditingStyle.Delete {
            
            // remove data in array at index given by indexPath
            toDoItems.removeAtIndex(indexPath.row)
            // store the updated list in memory
            let fixedToDoItems = toDoItems
            NSUserDefaults.standardUserDefaults().setObject(fixedToDoItems, forKey: "List")
            NSUserDefaults.standardUserDefaults().synchronize()
            
            // reload table
            tasksTable.reloadData()
        }
    }
}

